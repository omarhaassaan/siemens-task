# Untitled

# Pipeline Stages

![Pipeline Block Diagram.png](Pipeline_Block_Diagram.png)

This is a diagram for our whole continuous integration/continuous deployment (CI/CD) pipeline. Here are the details of the diagram:

- **Build Stage**: Contains a single block labeled "build-job," which represents the initial phase where the software build process takes place.
- **Test Stage**: This stage has two blocks. The primary block is labeled "downstream-job," indicating a job that likely takes the build artifacts from the build stage and performs further actions. There is a secondary block labeled "unit-test-job," which is connected to the "downstream-job" by a trigger. This suggests that the "unit-test-job" is a part of the testing phase, specifically for running unit tests, and it is initiated by the "downstream-job."
- **Deploy Stage**: The final stage has a single block labeled "deploy-job," which represents the deployment of the software after the build and test stages are successfully completed.

The arrows between the blocks indicate the flow of the process, starting from the build stage, moving to the test stage, and finally to the deploy stage, reflecting a typical sequence in automated software deployment pipelines.

Hidden job

`.common-features:`

A hidden job used, for me it was as inheritance in traditional programming languages, which for me is a scalable code, and an application for one of the OOP concepts actually for code reusability

Code commenting

description for each line except for the printing consoles for sure

Downstream job

`downstream-unit-testing-job:`

Was to get the job of the unit testing phase from another file to implement downstream concept which is mostly used for highly scalable pipelines

### Workaround for the issue

### Solution 1

Interruptible set to true

`interruptible: true` 

This for any new pipeline running on such project, the old ones should be canceled

Resource group

`resource_group: RESOURCE_GROUP`

Added a resource group to all the jobs, this resource group limit only one pipeline to use the same resource group at a given time, which is what was intended to do

### Solution 2

Lock branch

By adding an initial job to acquire the lock by creating the branch and checking whether it is already created. In case already created, the whole pipeline will fail or should wait for this branch to be deleted by the job created it.

Adding a release the lock job to delete the created branch.

![Locked Pipeline Block Diagram.png](Locked_Pipeline_Block_Diagram.png)
